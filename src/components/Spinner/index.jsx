import React from 'react'
import './index.scss'

export default ({ show }) => {
  if(show) {
    return (
      <div id="overlay">
        <div className="spinner"> </div>
      </div>
    )
  }
  return null
}