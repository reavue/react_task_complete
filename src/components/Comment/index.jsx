import React from 'react'
import { string } from 'prop-types'
import './index.scss'

const Comment = ({userName, src, text}) => (
  <div className="comment">
    <div className="avatar">
      <div className="avatar__box">
        <img src={src}/>
      </div>
    </div>
    <div className="comment__text">
      <span>{userName}</span>
      <p>{ text }</p>
    </div>
  </div>
)

Comment.propTypes = {

}

export default Comment