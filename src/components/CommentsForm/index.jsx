import React, { Component } from 'react'
import { FormGroup, FormControl, ControlLabel, Col} from 'react-bootstrap'


class CommentForm extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      value: ''
    };
  }

  getValidationState() {
    const length = this.state.value.length;
    if (length > 0 && length <= 25) return 'success';
    else if (length > 25) return 'error';
    return null;
  }

  onSubmit(e) {
    e.preventDefault()
    this.props.onSubmit(this.state.value)
    this.setState({ value: '' })
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <p>Comments</p>
        <Col sm={1} md={1} xs={1} style={{ textAlign: 'right' }}>
          <div className="avatar">
            <div className="avatar__box avatar__box--form">
              <img src="https://s3.amazonaws.com/uifaces/faces/twitter/mhesslow/128.jpg"/>
            </div>
          </div>
        </Col>
        <Col sm={11} md={11} xs={11}>
          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationState()}
          >
            <FormControl
              type="text"
              value={this.state.value}
              placeholder="Add a comment..."
              onChange={this.handleChange}
            />
            <FormControl.Feedback />
          </FormGroup>
        </Col>

      </form>
    );
  }
}

export default CommentForm