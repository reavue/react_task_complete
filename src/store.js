import {createStore, applyMiddleware, compose} from 'redux'
import { routerMiddleware } from 'react-router-redux'
import { hashHistory } from 'react-router'
import thunk from 'redux-thunk'
import rootReducer from './reducers'
let initialState = {};

const Store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(
      thunk,
      routerMiddleware(hashHistory))
  ),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);


export default Store;