import { combineReducers } from 'redux';
import commentsReducer from './comments';
import { routerReducer } from 'react-router-redux'

export default combineReducers({
  routing: routerReducer,
  comments: commentsReducer,
});