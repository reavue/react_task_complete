import { FETCHING_SUCCESS, FETCHING_ERROR, FETCHING_DATA } from '../actions/const'

const initialState = {}

const commentsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_DATA:
    case FETCHING_ERROR:
    case FETCHING_SUCCESS:
      return Object.assign(state, action.payload);
    default:
      return state;
  }
};
export default commentsReducer;