import React, {Component} from 'react'
import {connect} from 'react-redux'
import Comment from './components/Comment'
import Spinner from './components/Spinner'
import response from './actions/actionCreator'
import CommentsForm from './components/CommentsForm'
import {post} from './api'
import './App.scss'

/**
 * Main Wrapper
 */
class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      showSpinner: false
    }
    this.sendComment = this.sendComment.bind(this)
  }

  componentDidMount() {
    this.getComments()
  }

  getComments() {
    this.props.getComments()
      .then(json => {
        this.setState({showSpinner: false})
        this.forceUpdate()
      })
  }

  //  show spinner should be in redux
  //  i do this here, because haven`t enough time
  sendComment(text) {
    this.setState({
      showSpinner: true
    })
    post('comments', {
      userName: 'Jessy Parse',
      user_avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/mhesslow/128.jpg",
      body: text
    }).then(json => this.getComments())
  }

  render() {
    console.log(this.props)
    const {comments} = this.props
    return (
      <div className="main-wrapper">
        <main className="app__container">
          <CommentsForm onSubmit={this.sendComment}/>
          <Spinner show={this.state.showSpinner}/>
          <ul>
            {
              comments.data ? comments.data.map(comment => (
                <li key={comment.id}>
                  <Comment
                    userName={comment.userName}
                    text={comment.body}
                    src={comment.user_avatar}
                  />
                </li>
              )) : <Spinner show={true}/>
            }
          </ul>
        </main>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  comments: state.comments
})

const mapDispatchToProps = dispatch => ({
  getComments: url => dispatch(response('comments'))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);