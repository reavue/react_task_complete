import { CONTENT_TYPE_JSON } from './const';
const baseHost = 'http://localhost:3003'

const get = (url) =>
  fetch(`${baseHost}/${url}`)

const POST_CONFIG = {method: 'POST', headers: {'Content-Type': CONTENT_TYPE_JSON}};
const post = (url, body) =>
  fetch(`${baseHost}/${url}`, Object.assign(POST_CONFIG, { body: JSON.stringify(body) }));

export {
  get,
  post
}