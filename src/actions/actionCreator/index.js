import {
  FETCHING_DATA,
  FETCHING_ERROR,
  FETCHING_SUCCESS
} from '../const';
import { get, post } from '../../api'

//	when request is pending
const fetching = () => ({
  type: FETCHING_DATA,
  payload: {
    pending: true
  }
});

// 	response successful
const responseSuccess = (data) => ({
  type: FETCHING_SUCCESS,
  payload: {
    data,
    error: false,
    loaded: true,
    pending: false
  }
});

//	response fail
const responseFailure = error => ({
  type: FETCHING_ERROR,
  payload: {
    error,
    pending: false
  }
});

//	make request
const makeResponse = (url) => (dispatch, getState) => {

  //	show spinner
  dispatch(fetching());
  return get(url)
    .then(response =>
      response.json().then(data => {
        dispatch(responseSuccess(data));
      })
    )
    .catch(error => dispatch(responseFailure(error)));
};

export default makeResponse;

export {
  fetching,
  responseSuccess,
  responseFailure
};